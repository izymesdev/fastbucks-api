package com.izymes.jira.fastbucks.api;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericInvoice {
    private String invoiceNumber;
    private String contactName;
    private String contactEmail;
    private Date date;
    private List<GenericItem> items = new ArrayList<GenericItem>();

    public GenericInvoice addItem(Date date, String name, String desc, float quantity, float unitCost ){
        items.add( new GenericItem(date, name, desc, quantity, unitCost ));
        return this;
    }

    public List<GenericItem> getItems(){
        return items;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
}
